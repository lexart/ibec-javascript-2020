let app = angular.module('app', [])

app.controller('MainCtrl', ['$scope', '$http', function ($scope, $http){
	$scope.producto = {}
	$scope.productos = []

	// Hacer un GET hacia la API 
	$http.get(API + '/productos').then( function (res){
		$scope.productos = res.data;
	})

	$scope.guardar = function (){
		console.log($scope.producto);
		let producto = $scope.producto;

		if(!producto._id){
			$http.post(API + '/productos', producto).then( function (res){
				console.log("success: ", res)

				// Hacer un GET hacia la API 
				$http.get(API + '/productos').then( function (res){
					$scope.productos = res.data;
				})

			}, function (err){
				console.log("error: ", err)
			})
		} else {
			
			const id = producto._id;

			const productoEdit = {
				nombre: producto.nombre,
				precio: producto.precio,
				cantidad: producto.cantidad
			}
			
			$http.put(API + '/productos/' + id, productoEdit).then( function (res){
				console.log("success: ", res)

				// Hacer un GET hacia la API 
				$http.get(API + '/productos').then( function (res){
					$scope.productos = res.data;
				})

			}, function (err){
				console.log("error: ", err)
			})
		}
	};

	$scope.deleteProducto = function (id){
		if(confirm("Esta seguro que desea eliminar el producto?")){
			$http.delete(API + '/productos/' + id).then( function (res){
				console.log("res: ", res)

				// Hacer un GET hacia la API 
				$http.get(API + '/productos').then( function (res){
					$scope.productos = res.data;
				})
				
			}, function (err){
				console.log("Error: ", err);
			})
		}
	}

	$scope.selectProducto = function (producto){
		// Cast por que me llega como string las propiedades numericas
		producto.cantidad = parseInt(producto.cantidad);
		producto.precio   = parseFloat(producto.precio);

		$scope.producto = producto;
	}
}])