const formCont = $('#formProducto');
const btnForm  = formCont.find('button');
const tbody    = $('#listaProductos').find('tbody');

btnForm.click( function (){
	let inputs = formCont.find('input');
	let producto = {}

	for (let i = 0; i < inputs.length; i++) {
		let input = inputs[i];

		// i: 0 producto["nombre"] = "xxx"
		// i: 1 producto["precio"] = 123
		// i: 2 producto["cantidad"] = 123

		producto[input.name] = input.value;
	}
	// {nombre: "xxx", precio: 123, cantidad: 123}

	// Si no viene la propiedad "_id" entonces yo creo el producto
	if(!producto._id || producto._id == ""){

		// Elimina la propiedad de un objeto
		// Para que no me de error el servicio de crudcrud.com
		delete producto._id;

		$.ajax({
		  url: API + '/productos',
		  method: 'POST',
		  data: JSON.stringify(producto),
		  contentType: 'application/json',
		  dataType: 'json'
		}).done( function (res){
			alert("Producto creado correctamente!")
			window.location.reload();

		}).fail( function (err){
			alert("Error al crear producto.")
			window.location.reload();
		})
	} else {
		// Caso contrario voy a editar el  producto
		const id = producto._id;

		// Elimina la ID del producto
		delete producto._id;

		$.ajax({
		  url: API + '/productos/' + id,
		  method: 'PUT',
		  data: JSON.stringify(producto),
		  contentType: 'application/json',
		  dataType: 'json'
		}).done( function (res){
			alert("Producto actualizado correctamente!")
			window.location.reload();

		}).fail( function (err){
			// Crudcrud no devuelve respuesta del put
			// Entonces cae en el fail
			alert("Producto actualizado correctamente!")
			window.location.reload();
		})
	}

	console.log("Envio formulario ::", producto)
})


// Hacer un GET
// t = 2s
$.get(API + '/productos').then( function (res){
	let productos = res;
	let tpl 	  = ``
	for (let i = 0; i < productos.length; i++) {
		let producto = productos[i];

		tpl += `
			<tr>
				<td>${producto._id}</td>
				<td>${producto.nombre}</td>
				<td>${producto.precio}</td>
				<td>${producto.cantidad}</td>
				<td>
					<button onclick="obtenerProductoPorId('${producto._id}')">Editar</button>
					<button class="delete" id="${producto._id}">Eliminar</button>
				</td>
			</tr>
		`
	}
	// Incrustar el template en el tbody
	tbody.append(tpl);

	// Como eliminar desde jQuery
	$('.delete').click( function (){
		let id = $(this).attr('id');

		console.log("¿a quien elimino?", id)

		// Utilizar la ID que capture 
		// Y llamo la función eliminarProducto
		eliminarProducto(id)
	})

	console.log("productos :: ",res);
})


const eliminarProducto = function (id){
	if(confirm("Esta seguro que desea eliminar el producto?")){
		$.ajax({
		  url: API + '/productos/' + id,
		  method: 'DELETE'
		}).done( function (res){
			alert("Producto eliminado correctamente!")
			window.location.reload();

		}).fail( function (err){
			alert("Error al eliminar producto.")
			window.location.reload();
		})
	}
}

const obtenerProductoPorId = function (id){
	$.get(API + '/productos/' + id).then( function (res){
		let producto = res; // Este producto viene de la API

		let inputs = formCont.find('input');

		for (let i = 0; i < inputs.length; i++) {
			let input = inputs[i]

			// i: 0 input name="_id" ~ producto["_id"]
			// i: 1 input name="nombre" ~ producto["nombre"]
			// i: 2 input name="cantidad" ~ producto["cantidad"]
			// i: 3 input name="precio" ~ producto["precio"]

			input.value = producto[input.name];
		}
	})
}