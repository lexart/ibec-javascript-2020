let app = angular.module('app', [])

// Inicializo el controller
app.controller('MainCtrl', ['$scope', function ($scope) {
	$scope.titulo = "Hola Mundo!"
	$scope.persona   = {}
	$scope.personas  = []


	if(localStorage.getItem('_personas')){
		$scope.personas = JSON.parse(localStorage.getItem('_personas'));
	}

	$scope.isEdit = {
		edit: false,
		pos: null
	}

	$scope.guardar = function (){
		const persona = angular.copy($scope.persona);

		if($scope.isEdit.edit === true){
			// Defino la posición de donde quiero editar
			const pos = $scope.isEdit.pos;

			$scope.personas[pos] = persona;

		} else {
			$scope.personas.push(persona)
		}

		console.log("Todas las personas: ", $scope.personas)
		$scope.persona = {}
		$scope.isEdit = {
			edit: false,
			pos: null
		}

		$scope.guardarEnMemoria()
	}

	$scope.eliminar = function (pos){
		console.log("posicion: ", pos)
		$scope.personas.splice(pos, 1)

		$scope.guardarEnMemoria()
	}

	$scope.editar = function (obj, pos){
		$scope.persona = obj;

		$scope.isEdit = {
			edit: true,
			pos: pos
		}
	}

	$scope.guardarEnMemoria = function (){
		localStorage.setItem('_personas', JSON.stringify($scope.personas))
	}

}])