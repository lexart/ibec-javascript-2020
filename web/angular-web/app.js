let app = angular.module('app', []);

app.controller('MainCtrl', ['$scope', function ($scope){
	$scope.busqueda = ""

	$scope.resultados = []

	const socios = [
		{
			nombre: "Alex Casadevall",
			edad: 28,
			documento: 5100833,
			montoDisp: 10000,
			tienePres: false
		},
		{
			nombre: "Juan Correa",
			edad: 25,
			documento: 5101833,
			montoDisp: 10000,
			tienePres: true
		},
		{
			nombre: "Pedro Casadevall",
			edad: 38,
			documento: 5111833,
			montoDisp: 10000,
			tienePres: true
		}
	]

	$scope.buscar = function (){
		$scope.resultados = []
		let q = $scope.busqueda.toUpperCase();

		for (let i = 0; i < socios.length; i++) {
			let socio = socios[i];

			// i: 0 "JA" esta incluido en "ALEX CASADEVALL"
			// i: 1 "JA" esta incluido en "JUAN CORREA"
			// i: 2 "JA" esta incluido en "PEDRO CASADEVALL"
			if(socio.nombre.toUpperCase().includes(q)){
				// Sumo el socio al array resultados
				$scope.resultados.push(socio);
			}
		}

		console.log("resultados: ", $scope.resultados);
	}
}])