let app = angular.module('app', [])

app.controller('MainCtrl', ['$scope','$rootScope', function ($scope, $rootScope){

	$scope.vistasEstados = {
		"login":"views/login.html",
		"productos": "views/productos.html",
		"dashboard": "views/dashboard.html"
	}

	$rootScope.estado = 'login'

	$rootScope.cambioEstado = function (estadoFuturo){
		$rootScope.estado = estadoFuturo;
	}
}])