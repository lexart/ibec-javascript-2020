app.factory('ProductoService', ['$http', function ($http){
	return {
		// Tiene como parametro de entrada un objeto: que llamamos "producto"
		// Y devuelve una función: que llamamos "cb"
		nuevo: function (producto, cb){
			// Hace una petición a la API de tipo POST
			// Envía el objeto "producto" en el payload
			$http.post(API + '/productos', producto).then( function (res){

				// Voy a enviar para el controller 
				// La respuesta de la lista de productos nueva (o actualizada)
				// Si la respuesta es OK

				// Hago un GET de todos los productos 
				// Que tengo en la API
				$http.get(API + '/productos').then( function (res){

					// Libero los productos dentro de una función
					cb(res.data);
				})
				
			}, function (err){
				// Si me da algun error
				// ENvio ese objeto hacia el controller
				cb({error: err});
			})
		},
		actualizar: function (producto, cb){
			const id = producto._id;

			const productoEdit = {
				nombre: producto.nombre,
				precio: producto.precio,
				cantidad: producto.cantidad
			}
			
			$http.put(API + '/productos/' + id, productoEdit).then( function (res){

				// Hacer un GET hacia la API 
				$http.get(API + '/productos').then( function (res){
					cb(res.data);
				})

			}, function (err){
				cb({error: err});
			})
		},
		eliminar: function (id, cb){
			if(confirm("Esta seguro que desea eliminar el producto?")){
				$http.delete(API + '/productos/' + id).then( function (res){

					// Hacer un GET hacia la API 
					$http.get(API + '/productos').then( function (res){
						cb(res.data);
					})
					
				}, function (err){
					cb({error: err});
				})
			} else {
				cb({error: 'No se elimino nada'})
			}
		},
		todos: function (cb) {
			$http.get(API + '/productos').then( function (res){
				cb(res.data);
			})
		}
	}
}])