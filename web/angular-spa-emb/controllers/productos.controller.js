app.controller('ProductosCtrl', ['$scope','$http','ProductoService', function ($scope, $http, ProductoService){
	$scope.producto = {}
	$scope.showForm = false;

	$scope.formVisible = function (){
		$scope.showForm = true;
		$scope.producto = {}
	}

	$scope.formHide    = function (){
		$scope.showForm = false;
	}

	console.log("ProductoService: ", ProductoService)

	$scope.productos = []

	// Hacer un GET hacia la API 
	ProductoService.todos( function (res){
		$scope.productos = res;
	})

	$scope.guardar = function (){
		let producto = $scope.producto;

		if(!producto._id){
			ProductoService.nuevo(producto, function (res){
				if(!res.error){
					$scope.productos = res;
					$scope.formHide()
				}
			})
		} else {
			ProductoService.actualizar(producto, function (res){
				if(!res.error){
					$scope.productos = res;
					$scope.formHide()
				}
			})
		}
	};

	$scope.deleteProducto = function (id){
		ProductoService.eliminar(id, function (res){
			if(!res.error){
				$scope.productos = res;
			}
		})
	}

	$scope.selectProducto = function (producto){
		$scope.formVisible()
		
		// Cast por que me llega como string las propiedades numericas
		producto.cantidad = parseInt(producto.cantidad);
		producto.precio   = parseFloat(producto.precio);

		$scope.producto = producto;
	}
}])