let productos = []

// Si existe en memoria del navegador los productos
if(localStorage.getItem('_productos')){
	productos = JSON.parse( localStorage.getItem('_productos') );
}

let tbody =  document.querySelector('#productosTable .table tbody')

// La función buildProductos
// va a tener un parametro de entrada: array de productos "arr"
// la salida será un template
const buildProductos = function (arr){
	// Vamos a crear un template por c/objeto del array 
	let tr 	  = ``

	// Recorrer productos
	for (let i = 0; i < arr.length; i++) {
		let producto = arr[i]

		tr += `<tr>
					<td>${producto.id}</td>
					<td>${producto.nombre}</td>
					<td>${producto.precio}</td>
					<td>${producto.stock}</td>
					<td>${producto.moneda}</td>
					<td>
						<button class="btn btn-primary" onclick="obtenerProducto(${producto.id})">Editar</button>
						<button class="btn btn-danger" onclick="eliminarProducto(${producto.id})">Eliminar</button>
					</td>
				</tr>`
	}
	return tr;
}

// Ingresando el template resultante para dentro del tbody
tbody.innerHTML = buildProductos(productos);


const obtenerProducto = function (id){
	console.log("obteniendo id correcto : ", id)

	// Procedimiento de editar:
	// 1. Obtengo la ID del producto
	// 2. Busco el producto dentro del array "productos" y almaceno en una variable "producto"
	// 3. Agrego ese producto al formulario
		// 3.1 Obtengo los inputs
		// 3.2 Obtengo el select
		// 3.3 Asigno los valores input ~ value | select ~ value
	// 4. Oculto la tabla y muestro el formulario

	// Punto 2.
	let producto;

	for (let i = 0; i < productos.length; i++) {
		if(id == productos[i].id){
			producto = productos[i];

			// Si encuentro corto la ejecución
			break;
		}
	}

	// Punto 3.
	// 3.1 Obtengo los inputs
	let inputs = document.querySelectorAll('#formProduct form input');

	// 3.2 Obtengo el select
	let select = document.querySelector('#formProduct form select');

	// 3.3 Asigno los valores input ~ value | select ~ value
	inputs[0].value = producto.nombre
	inputs[1].value = producto.precio
	inputs[2].value = producto.stock

	// Asignar ID al input de tipo hidden
	inputs[3].value = producto.id

	select.value    = producto.moneda

	// Punto 4.
	showForm()
}


const guardarProducto = function (){
	let inputs = document.querySelectorAll('#formProduct form input');

	// Primer input: nombre
	let nombre = inputs[0].value;

	// Segundo input: precio
	let precio = inputs[1].value;

	// Tercer input: stock
	let stock  = inputs[2].value;

	// Cuarto: select moneda
	let select = document.querySelector('#formProduct form select');
	let moneda = select.value;

	// Quinto: obtener la ID si lo tiene
	let id 	   = inputs[3].value;

	let producto = {}

	if(id != ''){
		// Lo edita
		producto = {
			id: id,
			nombre: nombre,
			precio: precio,
			stock: stock,
			moneda: moneda
		}

		for (let i = 0; i < productos.length; i++) {
			if(producto.id == productos[i].id){

				// Remplazar el producto del array
				productos[i] = producto;
				break;
			}
		}
	} else {

		// Construir un objeto: producto (nombre, precio, stock, moneda)
		producto = {
			id: randId(),
			nombre: nombre,
			precio: precio,
			stock: stock,
			moneda: moneda
		}

		// Agregar a la lista de productos el objeto "producto"
		productos.push(producto);
	}

	console.log("producto: ", producto)
	console.log("arr productos actual: ", productos)

	// Procedimiento:
	// 1. Re armar la tabla de productos con el array productos
	// 2. Luego ocultar el formulario y mostrar la tabla
	// 3. Vaciar los campos del formulario

	// Guardar el array en memoria
	localStorage.setItem('_productos', JSON.stringify(productos) );

	// Punto 1:
	// Llamamos la función "buildProductos"
	tbody.innerHTML = buildProductos(productos);

	// Punto 2:
	// Llamamos la función "hideForm"
	hideForm()

	// Punto 3: 
	inputs[0].value 	= ""
	inputs[1].value 	= ""
	inputs[2].value  	= ""
	inputs[3].value  	= ""
	select.value 		= ""

}

const eliminarProducto = function (id){
	if(confirm('Esta seguro que desea eliminar este producto?')){
		console.log("eliminar producto id ::", id)

		// Encuentro el producto que deseo eliminar por ID
		// Luego le hago un splice
		for (let i = 0; i < productos.length; i++) {
			if(id == productos[i].id){

				// i: Indice
				// 1, cantidad de items a eliminar a partir del indice
				productos.splice(i,1);

				// Si encuentro corto la ejecución
				break;
			}
		}

		// Guardar el array en memoria
		localStorage.setItem('_productos', JSON.stringify(productos) );

		// Llamamos la función "buildProductos"
		tbody.innerHTML = buildProductos(productos);
	} else {
		// ...
	}
}

// Función cerrar sesión: 
const cerrarSesion  = function () {
	// Redireccionar al index.html

	location.href="index.html"
}

const showForm = function (){
	// Ocultamos la tabla
	document.querySelector('#productosTable').style.display = "none"

	// Mostramos el formulario 
	document.querySelector('#formProduct').style.display = ""
}

const hideForm = function (){
	// Ocultamos la tabla
	document.querySelector('#productosTable').style.display = ""

	// Mostramos el formulario 
	document.querySelector('#formProduct').style.display = "none"
}

const randId = function (){
	let size = 1000000
	let rand = Math.random()

	return Math.round(rand * size);
}
