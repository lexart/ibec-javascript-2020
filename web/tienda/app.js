console.log("document: ", document)

const USER = "alex"
const PSW  = "123"

// Se que el document es el vínculo entre el HTML ~ JS
// Necesito obtener datos de los input's

// el método document.querySelectorAll('input') ~ Esto me trae todos los inputs del HTML
// document.querySelectorAll('#loginForm input') ~ Esto trae todos los inputs hijos del loginForm

// Por que utiliza la misma notación de CSS
// contenedor B es hijo de contenedor A con la siguiente regla: 
// .a .b { regla css }

console.log("inputs del form: ", document.querySelectorAll('#loginForm input'))

let usuario = document.querySelectorAll('#loginForm input')[0]
let clave 	= document.querySelectorAll('#loginForm input')[1]

// settear usuario con alex
// settear clave con 123

usuario.value = "pepe"
clave.value   = "hola"


const ingresar = function (){
	let usuario = document.querySelectorAll('#loginForm input')[0]
    let clave 	= document.querySelectorAll('#loginForm input')[1]

    if(usuario.value == USER && clave.value == PSW){
    	
    	// Redireccionar a la pagina "productos.html"
    	location.href="productos.html"
    
    } else {
    	alert("Error usuario y/o clave incorrecta.")
    }

    console.log("usuario: ", usuario.value)
    console.log("clave: ", 	 clave.value)
}
