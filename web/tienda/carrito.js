// Definimos un array para utilizar en el template
// Por defecto es vacío
let carrito = []
let cartContTable = document.querySelector('#cartContainer table tbody');
let cartTotal 	  = document.querySelector('#cartTotal');

// Preguntamos si existe elementos en el carrito
if(localStorage.getItem('_carrito')){

	// Convirtiendo el JSON que estaba en memoria en un array tangible
	carrito = JSON.parse(localStorage.getItem('_carrito'))
}

const delFromCart = function (idProducto){
	for (let i = 0; i < carrito.length; i++) {
		let prod = carrito[i]

		if(prod.id == idProducto){
			// Elimino el elemento desde el indicie: i
			// Y la cantidad de elementos a partir de el
			carrito.splice(i, 1);
			break;
		}
	}

	localStorage.setItem('_carrito', JSON.stringify(carrito));

	// Corremos el procedimiento: procCarrito
	procCarrito()
}

const procCarrito = function (){
	let total 	= 0;
	let moneda  = "$ ";

	let tr = ``
	for (let i = 0; i < carrito.length; i++) {
		let producto = carrito[i];

		let subTotal = parseInt(producto.cantidad) * parseInt(producto.precio)
		
		// La sumatoria de los subtotales 
		total += subTotal;

		tr += `<tr>
			<td>${producto.id}</td>
			<td>${producto.nombre}</td>
			<td>${producto.moneda} ${producto.precio}</td>
			<td>${producto.cantidad}</td>
			<td>${producto.moneda} ${subTotal}</td>
			<td>
				<button class="btn btn-danger" onclick="delFromCart(${producto.id})">Eliminar</button>
			</td>
		</tr>`
	}

	// Incrustar el HTML generado en el cartContTable
	cartContTable.innerHTML = tr;

	// Agregar el total como un texto
	cartTotal.innerText 	= moneda + total;

}
procCarrito();
