let tiendaCont    = document.querySelector('#tiendaContainer');
let cantidadCont  = document.querySelector('#cantidad');
let productos  = []

if(localStorage.getItem('_productos')){
	productos = JSON.parse( localStorage.getItem('_productos') );
}

let tplProducto = ``
let imgPlaceholder = 'https://cdn.shopify.com/s/files/1/0533/2089/files/placeholder-images-image_large.png?format=jpg&quality=90&v=1530129081';

for (let i = 0; i < productos.length; i++) {
	let producto = productos[i];

	tplProducto += `
		<div class="compProducto">
			<!-- Imagen del producto -->
			<img src="${imgPlaceholder}">
			<!-- Nombre del producto -->
			<p>${producto.nombre} - ${producto.moneda} ${producto.precio}</p>
			<div class="row">
				<input id="cantidad-${producto.id}" type="number" min="0" value="0" name="cantidad" placeholder="Cantidad" class="col-6 form-control productoCantidad">
				<button class="btn btn-primary col-4" onclick="addToCart(${producto.id})">Agregar</button>
			</div>
		</div>
	`
}

// Conectar ese HTML dinámico que se creo a partir del array de productos
tiendaCont.innerHTML = tplProducto;

let cantidad = 0;
let carrito  = []

const procCantidad = function (){
	cantidad = 0;
	
	for (let i = 0; i < carrito.length; i++) {
		cantidad += carrito[i].cantidad;
	}
}

// Preguntamos si existe elementos en el carrito
if(localStorage.getItem('_carrito')){

	// Convirtiendo el JSON que estaba en memoria en un array tangible
	carrito = JSON.parse(localStorage.getItem('_carrito'))

	// Correr el procedimiento de calcular cantidades del carrito
	procCantidad();

	// Y agregar la cantidad en el icono
	cantidadCont.innerText = cantidad;
}

const addToCart = function (idProducto){
	let inputCantidad = document.querySelector('#cantidad-'+idProducto).value;

	console.log("inputCantidad: ", inputCantidad)

	// Agregar los productos que selecciono y llenar el array del carrito
	// 1. Buscar el producto por ID en el array de productos
	// 2. Hacer un carrito.push(...) del objeto encontrado para dentro del array carrito
	// 3. Guardar en memoria (localStorage)

	// Preguntar si el producto esta en el carrito
	let findCarrito = false;

	// Verifico si esta en el carrito y actualizo la cantidad
	// En caso de que encuentre el producto
	for (var i = 0; i < carrito.length; i++) {
		let prod = carrito[i]

		if(prod.id == idProducto){
			prod.cantidad += parseInt(inputCantidad);
			findCarrito = true;
			break;
		}
	}

	if(!findCarrito){
		// Buscar el producto
		for (let i = 0; i < productos.length; i++) {
			let prod = productos[i]

			// 1. Encuentra el producto dentro del array por idProducto
			if(prod.id == idProducto){
				
				// Asigno la cantidad al producto seleccionado
				prod.cantidad = parseInt(inputCantidad);

				// 2. Agrega el producto al array carrito
				carrito.push(prod);
				break;
			}
		}
	}

	console.log("arr carrito: ", carrito)

	// Contando la cantidad de productos que se suma al carrito
	// cantidad += parseInt(inputCantidad);

	// Corro el procedimiento c/vez que agrego un producto al carrito 
	procCantidad()

	cantidadCont.innerText = cantidad;

	// Guardar en memoria
	localStorage.setItem('_carrito', JSON.stringify(carrito))
}
