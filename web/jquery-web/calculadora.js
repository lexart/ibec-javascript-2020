// t=1

// Evento
// Selector + función que retorna una función
// t=x ~ Disparo el evento
$('#resultado').click( function (){
	let valorNuevo = 0;
	// Estamos asumiendo que nuestras operaciones no tienen error
	let error 	   = false;

	// t=x + 1
	let num1 = $('#num1').val()
	let num2 = $('#num2').val()
	// El select dentro del div con: id="calculadora"
	let operacion = $('#calculadora select').val()

	// Me llegan los numeros como un string 
	// Convertir en float
	num1 = parseFloat(num1)
	num2 = parseFloat(num2)

	if(isNaN(num1) || isNaN(num2)){
		error = true;
	}

	// Verificamos si la operacion es: suma
	// if(operacion == "suma"){
	// 	valorNuevo = num1 + num2;
	// } else if (operacion == "resta"){
	// 	valorNuevo = num1 - num2;
	// } else if (operacion == "multi"){
	// 	valorNuevo = num1 * num2;
	// } else {

	// 	// Veriricar caso borde de la división
	// 	if(num2 != 0){
	// 		valorNuevo = num1 / num2;
	// 	} else {
	// 		error = true;
	// 	}
	// }
	
	let result;
	
	try {
		result = opService[operacion](num1, num2)
	} catch(e){
		result = 'Err'
	}

	if(result == 'Err'){
		error = true;
	} else {
		valorNuevo = result;
	}

	// Tiene dos cifras significativas luego de la coma
	valorNuevo = valorNuevo.toFixed(2);

	// Estamos asumiendo si la operación falla
	if(error){
		// Asumimos que pudo haber un success
		$('#calculadora p').removeClass('success');

		$('#calculadora p').addClass('error');
		$('#calculadora b').text('Error de operacion');
	} else {
		// Asumimos que pudo haber un error
		$('#calculadora p').removeClass('error');

		$('#calculadora p').addClass('success');
		$('#calculadora b').text(valorNuevo)
	}
})
