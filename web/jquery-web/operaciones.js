let operaciones = [
	{
		nombre:"Suma",
		valor:"suma"
	},
	{
		nombre:"Resta",
		valor:"resta"
	},
	{
		nombre:"Multiplicación",
		valor:"multi"
	},
	{
		nombre:"División",
		valor:"divi"
	},
	{
		nombre:"Modulo",
		valor:"mod"
	},
	{
		nombre:"Op - Alex",
		valor:"alex"
	},
	{
		nombre:"Op2 - Alex",
		valor:"alex2"
	}
]

// Insertar dentro del select
for (let i = 0; i < operaciones.length; i++) {
	let operacion = operaciones[i]
	let tpl = `
		<option value="${operacion.valor}">${operacion.nombre}</option>
	`
	// Incrustar en el HTML (select) los options
	$('#calculadora select').append(tpl);
}