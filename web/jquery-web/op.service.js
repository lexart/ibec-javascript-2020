const opService = {
	suma: function (a, b){
		return a + b;
	}, 
	resta: function (a, b){
		return a - b;
	},
	multi: function (a, b){
		return a * b;
	},
	divi: function (a, b){
		if(b != 0){
			return a / b;
		} else {
			return 'Err';
		}
	},
	mod: function (a, b){
		return a % b;
	},
	alex: function (a, b){
		return (a + b) * (a - b);
	}
}